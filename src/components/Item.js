import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';

export default withRouter(class extends Component {
    constructor() {
        super();
        this.state = {
            item: null
            
        }
    }
    async componentDidMount(){
        let {data} = await axios.get(`/api/list/item/${this.props.match.params.id}`, {
            baseURL: 'http://127.0.0.1:3000'
        })
        this.setState({item: data.data});
    }
    render() {
        let self = this;
        return (
            <div className={"itemPage"} style={
                {
                    display: 'flex',

                }
            }>
                <img src={`http://127.0.0.1:3000${self?.state?.item?.image}`}/>
                <h2>{self?.state?.item?.title}</h2>
                <h5>{self?.state?.item?.body}</h5>
            </div>
        )
    }
})