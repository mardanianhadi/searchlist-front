import {Link} from 'react-router-dom';
import React, { Component } from 'react';
import axios from 'axios';

export default class extends Component {
    constructor() {
        super();
        this.state = {
            list: []
        }
    }

    async componentDidMount() {
        let { data } = await axios.get('/api/list/all', {
            baseURL: 'http://127.0.0.1:3000'
        });
        console.log(data);
        this.setState({ list: data.data });
    }
    render() {
        let self = this;
        let i = 0;
        return <ul className="cards" style={{
            listStyle: 'none'
        }}>

            {
                self.state?.list?.map(item => {
                    return (
                        <li key={i++}>
                            <Link to={{
                                pathname: `/item/${item._id}`,
                                state: {data: item},
                                
                            }} >
                                <div class="card">
                                    <h2 class="card-title">{item.title}</h2>
                                    <img src={`http://127.0.0.1:3000${item.image}`} alt="" />
                                    {/* <a href="#" className="navigate">Click me</a> */}
                                </div>
                            </Link>
                        </li>
                    )
                })
            }
        </ul>
    }
}