import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { List, Item } from './components/index.js';
import './App.css';
import './card.css'

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/">
          <List />
        </Route>
        <Route path="/item/:id">
          <Item />
        </Route>
      </Switch>
    </Router>
  );
}



export default App;
